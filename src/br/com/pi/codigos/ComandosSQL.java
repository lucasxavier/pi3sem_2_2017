package br.com.pi.codigos;

import java.sql.SQLException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import br.com.pi.telas.Guilda;
import br.com.pi.telas.Quests;

/**
 * Classe respons�vel por executar as principais consultas do banco MySQL
 * @author Lucas
 *
 */
public class ComandosSQL {

	public static String VOUCHER = "MASTER";
	public String sql ="";
	static GerenciadorBD bd = new GerenciadorBD();
	/**
	 * M�todo respons�vel por realizar o login no sistema, reconhecendo o email e senha
	 * cadastrados no banco MySQL
	 * @param login email a ser reconhecido no BD
	 * @param senha senha a ser reconhecida no BD
	 */
	public static void LogarUsuario(String login, String senha) {

		if(bd.getConnection()){
			//mensagem via console avisando que o sistema conseguiu se conectar						
			try{
				//realiza consulta na tabela procurando pelo email e senha
				String sql = "SELECT * FROM USUARIO";
				bd.st=bd.con.prepareStatement(sql);
				bd.rs=bd.st.executeQuery();

				while(bd.rs.next()){

					/*se a senha e usu�rio inseridos no Password Field e Text Field forem iguais
					aos existentes na tabela a cada loop do while, o sistema loga e vai para
					o menu principal*/
					if(senha.equals(bd.rs.getString("senha"))&&login.equals(bd.rs.getString("email"))) {
						JOptionPane.showMessageDialog(null, "Logado com sucesso!");
						GerenciadorTelas.MudarLabels(bd.rs.getString("nomeUsuario"), bd.rs.getString("levelUsuario"));
						GerenciadorTelas.AbrirMenu();
						GerenciadorTelas.BotaoQuest(bd.rs.getInt("serProf"));
						
					}
					else {
						if(bd.rs.next()==false) {
							JOptionPane.showMessageDialog(null, "CADASTRO INEXISTENTE \n ou USU�RIO/SENHA INCORRETO(S)");
						}
					}
				}
			}
			catch(SQLException erro){
				System.out.println(erro.toString());
			}			
			bd.close();
		}
		else
		{
			System.out.println("error");
		}	
	}
/**
 * M�todo respons�vel por exibir todos os usu�rios na tabela, por ordem decrescente de pontos (do maior
 * pontuador para o menor)
 */
	public static void GerarRankingGuilda() {
		if(bd.getConnection()){
			System.out.println("logged");

			try{
				String sql = "SELECT * FROM USUARIO ORDER BY totalPontos DESC";
				bd.st=bd.con.prepareStatement(sql);
				bd.rs=bd.st.executeQuery();
				DefaultTableModel tabela =(DefaultTableModel) Guilda.table.getModel();
				tabela.setRowCount(0);

				//enquanto tiver linha, ele executa
				while(bd.rs.next()){
					if(bd.rs.getInt("serProf")==0) {
						tabela.addRow(new Object[]
								{
										bd.rs.getString("nomeUsuario"),
										bd.rs.getString("totalPontos")
								});
					}
				}
			}
			catch(SQLException erro){
				System.out.println(erro.toString());
			}


			bd.close();
		}
		else
		{
			System.out.println("error");
		}	
	}
/**
 * M�todo respons�vel por criar uma nova quest
 * @param nQuest - Nome da quest
 * @param dQuest - descri��o da quest
 * @param difQuest - dificuldade da quest
 * @param pQuest - pontos da quest
 * @param sQuest - status da quest
 * @param idMatCurso - de qual mat�ria a quest faz parte
 */
	public static void CriarQuest(String nQuest, String dQuest, String difQuest, int pQuest, String sQuest, int idMatCurso) {
		if(bd.getConnection()){
			System.out.println("logged");

			try{
				String sql = "INSERT INTO QUEST (nomeQuest, descrQuest, dificQuest, pontosQuest, statusQuest,\r\n" + 
						"                   idMateriaCurso) VALUES (?,?,?,?,?,?) ";
				bd.st=bd.con.prepareStatement(sql);	
				bd.st.setString(1, nQuest);
				bd.st.setString(2, dQuest);
				bd.st.setString(3, difQuest);
				bd.st.setInt(4, pQuest);
				bd.st.setString(5, sQuest);
				bd.st.setInt(6, 1);
				int n =bd.st.executeUpdate();
				JOptionPane.showMessageDialog(null, "Quest criada com sucesso");
			}
			catch(SQLException erro){
				System.out.println(erro.toString());
			}


			bd.close();
		}
		else
		{
			System.out.println("error");
		}	
	}
/**
 * M�todo respons�vel por exibir todas as quests em uma tabela
 */
	public static void VisualizarQuests() {
		if(bd.getConnection()){
			System.out.println("logged");

			try{
				String sql = "SELECT * FROM QUEST ORDER BY idQuest ASC";
				bd.st=bd.con.prepareStatement(sql);
				bd.rs=bd.st.executeQuery();
				DefaultTableModel tabela =(DefaultTableModel) Quests.table.getModel();
				tabela.setRowCount(0);

				//enquanto tiver linha, ele executa
				while(bd.rs.next()){
					tabela.addRow(new Object[]
							{
									bd.rs.getString("nomeQuest"),
									bd.rs.getString("descrQuest"),
									bd.rs.getString("dificQuest"),
									bd.rs.getString("statusQuest")
							});
				}
			}
			catch(SQLException erro){
				System.out.println(erro.toString());
			}


			bd.close();
		}
		else
		{
			System.out.println("error");
		}	
	}
/**
 * M�todo respons�vel por apagar uma quest selecionada do banco
 */
	public static void ApagarQuest() {

	}
/**
 * M�todo respons�vel por editar uma quest j� adicionada
 */
	public static void EditarQuest() {
		if(bd.getConnection()){
			System.out.println("logged");

			try{
				String sql = "SELECT * FROM QUESTS ";
				bd.st=bd.con.prepareStatement(sql);
				bd.rs=bd.st.executeQuery();
				DefaultTableModel tabela =(DefaultTableModel) Guilda.table.getModel();
				tabela.setRowCount(0);

				bd.rs.getString("nomeQuest");


				//enquanto tiver linha, ele executa
				while(bd.rs.next()){
					tabela.addRow(new Object[]
							{
									bd.rs.getString("nomeUsuario"),
									bd.rs.getString("totalPontos")
							});
				}
			}
			catch(SQLException erro){
				System.out.println(erro.toString());
			}


			bd.close();
		}
		else
		{
			System.out.println("error");
		}	
	}
/**
 * M�todo respons�vel por criar um novo usu�rio
 * @param nome - nome do aluno/professor
 * @param email - email para login
 * @param voucher - campo especial para definir a conta com privil�gios de professor, se 
 * usada a palavra MASTER
 * @param senha - senha para login
 */
	public static void CadastrarUser(String nome, String email, String voucher, String senha) {
		if(bd.getConnection()){
			//mensagem via console avisando que o sistema conseguiu se conectar						
			try{

				String sql = "SELECT email FROM USUARIO WHERE email=?";
				bd.st=bd.con.prepareStatement(sql);
				bd.st.setString(1, email);
				bd.rs=bd.st.executeQuery();

				boolean found = false;


				while(bd.rs.next()){
					found = true;
					break;
				}

				if(found==false) {
					if(voucher.equals(VOUCHER)) {	
						sql = "insert into usuario (serProf, nomeUsuario, idCurso, levelUsuario, totalPontos, idMateriaCurso, email, senha) values (?, ?, '1', '1', '0', '1', ?, ?)";
						bd.st=bd.con.prepareStatement(sql);	
						bd.st.setString(2, nome);
						bd.st.setString(3, email);
						bd.st.setString(4, senha);
						bd.st.setInt(1, 1);
						int n =bd.st.executeUpdate();								
						JOptionPane.showMessageDialog(null, "Cadastro de professor(a) completo!");
						bd.close();
					}else{
						sql = "insert into usuario (serProf, nomeUsuario, idCurso, levelUsuario, totalPontos," + 
								" idMateriaCurso, email, senha) " + 
								"values " + 
								"(?, ?, '1', '1', '0', '1', ?, ?)";
						bd.st=bd.con.prepareStatement(sql);	
						bd.st.setString(2, nome);
						bd.st.setString(3, email);
						bd.st.setString(4, senha);
						bd.st.setInt(1, 0);
						int n =bd.st.executeUpdate();								
						JOptionPane.showMessageDialog(null, "Cadastro de aluno(a) completo!");
						bd.close();
					}
				}else {
					JOptionPane.showMessageDialog(null, "Usu�rio j� cadastrado!");
					bd.close();
				}
			}
			catch(SQLException erro){
				System.out.println(erro.toString());
			}			
			bd.close();
		}
		else
		{
			System.out.println("error");
		}	
	}
}
