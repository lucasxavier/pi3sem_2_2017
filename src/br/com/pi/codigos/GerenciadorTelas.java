package br.com.pi.codigos;

import br.com.pi.telas.*;
import br.com.pi.codigos.*;

public class GerenciadorTelas {
	static GerenciadorBD bd = new GerenciadorBD();
	static Guilda fGuild = new Guilda();
	static MainMenu fMain = new MainMenu();
	static Login fLogin = new Login();
	static Cadastro fCadastro = new Cadastro();
	static Quests fQuests = new Quests();
	static NovaQuests fnQuests = new NovaQuests();
/**
 * m�todo respons�vel por abrir a tela de quests e esconder as outras
 */
	public static void AbrirQuests() {
		fQuests.setVisible(true);
		fMain.setVisible(false);
	}
/**
 * m�todo respons�vel por abrir a tela de guilda e esconder as outras
 */
	public static void AbrirGuilda() {
		fGuild.setVisible(true);
		fMain.setVisible(false);
	}
/**
 * m�todo respons�vel por abrir a tela de menu e esconder as outras
 */
	public static void AbrirMenu() {
		fMain.setVisible(true);
		fLogin.setVisible(false);
		fQuests.setVisible(false);
		fGuild.setVisible(false);
		fnQuests.setVisible(false);
	}
/**
 * m�todo respons�vel por encerrar a conex�o com o banco e fechar a aplica��o
 */
	public static void SairPrograma() {
		bd.close();
		System.exit(0);	
	}	
/**
 * m�todo respons�vel por esconder o main menu e exibir a tela de quests
 */
	public static void CadastrarQuest() {
		fMain.setVisible(false);
		fQuests.setVisible(false);
		fnQuests.setVisible(true);
	}
/**
 * m�todo respons�vel por esconder a tela de login e exibia tela de cadastro
 */
	public static void AbrirCadastro() {
		fCadastro.setVisible(true);
		fLogin.setVisible(false);
	}
/**
 * m�todo respons�vel por esconder a tela de cadastro e reexibir a de login
 */
	public static void AbrirLogin() {
		//apenas inserido para o bot�o Voltar da tela de cadastro sumir com a mesma
		fCadastro.setVisible(false);
		fLogin.setVisible(true);
	}
/**
 * m�todo respons�vel por exibir o nome do usu�rio e seu lvl nos labels presentes no Main Menu
 * @param nome - nome do usu�rio presente no BD
 * @param level - o n�vel do usu�rio
 */
	public static void MudarLabels(String nome, String level) {
		fMain.lblUser.setText("Bem-vindo(a): "+nome);
		fMain.lblLevel.setText("Level: "+level);
	}
/**
 * m�todo respons�vel por exibir ou n�o o bot�o de criar quests
 * @param i - se 1, exibe o bot�o. Se 0, o deixa oculto
 */
	public static void BotaoQuest(int i) {
		if(i==1) {
			Quests.btnCriarQuest.setVisible(true);
		}else {
			Quests.btnCriarQuest.setVisible(false);
		}
	}
}
