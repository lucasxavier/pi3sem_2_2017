package br.com.pi.codigos;

import java.sql.*;

public class GerenciadorBD {
	
	//conex�o
		public Connection con = null;
		//executa instru��es SQL
		public PreparedStatement st = null;
		//
		public ResultSet rs = null;
		
		public final String LOGIN = "root";
		public final String SENHA = "admin";
		public final String DATABASE = "piolingo";
		public final String DRIVER = "com.mysql.jdbc.Driver";
		public final String URL = "jdbc:mysql://localhost:3306/"+DATABASE;
		/**
		 * m�todo respons�vel por tentar se conectar no banco, retornando se foi bem-ucedido ou n�o
		 * @return - true se consewguiu se conectar, false se houve erro
		 */
		public boolean getConnection(){
			
			try{
				Class.forName(DRIVER);
				
				con = DriverManager.getConnection(URL, LOGIN, SENHA);
				System.out.println("driver loaded");
				return true;
			}
			catch(ClassNotFoundException erro){
				System.out.println("driver not found");
				return false;
			}
			catch(SQLException erro){
				System.out.println("connection failed"+erro.toString());
				return false;
			}
		}
		/**
		 * M�todo respons�vel por encerrar a conex�o com o banco
		 */
		public void close(){
			try{
				if(rs!=null){
					rs.close();
				}
			}
			catch(SQLException erro){
				System.out.println("erro");
			}
			
			try{
				if(st!=null){
					st.close();
				}
			}
			catch(SQLException erro){
				System.out.println("erro");
			}
			
			try{
				if(con!=null){
					System.out.println("disconnected");
					con.close();
				}
			}
			catch(SQLException erro){
				System.out.println("erro");
			}
		}

}
