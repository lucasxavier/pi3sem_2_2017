package br.com.pi.telas;

import br.com.pi.codigos.ComandosSQL;
import br.com.pi.codigos.GerenciadorTelas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.JDesktopPane;
import java.awt.Canvas;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainMenu extends JFrame {

	private JPanel contentPane;
	public JLabel lblUser = new JLabel("labelNorme");
	public JLabel lblLevel = new JLabel("lblNivel");

	/**
	 * Create the frame.
	 */
	public MainMenu() {
		setTitle("Piolingo - Main Menu");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainMenu.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 490, 260);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//JLabel lblUser = new JLabel("labelNorme");
		lblUser.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUser.setBounds(10, 11, 365, 30);
		contentPane.add(lblUser);
		
		//JLabel lblLevel = new JLabel("lblNivel");
		lblLevel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLevel.setBounds(10, 40, 200, 30);
		contentPane.add(lblLevel);
		
		JButton btQuests = new JButton("");
		btQuests.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GerenciadorTelas.AbrirQuests();
				ComandosSQL.VisualizarQuests();
			}
		});
		btQuests.setToolTipText("Ver as quests ativas");
		btQuests.setIcon(new ImageIcon(MainMenu.class.getResource("/br/com/pi/imagens/icoVerQuest.png")));
		btQuests.setBounds(231, 11, 180, 70);
		contentPane.add(btQuests);
		
		JButton btGuilda = new JButton("");
		btGuilda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GerenciadorTelas.AbrirGuilda();
				ComandosSQL.GerarRankingGuilda();
			}
		});
		btGuilda.setIcon(new ImageIcon(MainMenu.class.getResource("/br/com/pi/imagens/icoGuilda.png")));
		btGuilda.setToolTipText("Ver a guilda onde voc\u00EA est\u00E1 participando");
		btGuilda.setBounds(231, 92, 180, 70);
		contentPane.add(btGuilda);
		
		JButton btnFechar = new JButton("SAIR");
		btnFechar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GerenciadorTelas.SairPrograma();
			}
		});
		btnFechar.setBounds(32, 103, 89, 23);
		contentPane.add(btnFechar);
		
		JLabel lblBackground = new JLabel("");
		lblBackground.setHorizontalAlignment(SwingConstants.CENTER);
		lblBackground.setIcon(new ImageIcon("S:\\eclipse-workspace\\pi3sem_2_2017\\src\\br\\com\\pi\\imagens\\oldPaper.jpg"));
		lblBackground.setBounds(0, 0, 474, 225);
		contentPane.add(lblBackground);
	}
	

}
