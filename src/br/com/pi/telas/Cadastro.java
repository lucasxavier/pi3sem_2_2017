package br.com.pi.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.pi.codigos.*;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Cadastro extends JFrame {

	private JPanel contentPane;
	private JTextField tfNome;
	private JTextField tfEmail;
	private JTextField tfSenha;
	private JTextField tfVoucher;

	/**
	 * Create the frame.
	 */
	public Cadastro() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Cadastro.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setTitle("Piolingo - Cadastro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 294, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNome.setBounds(25, 25, 46, 14);
		contentPane.add(lblNome);
		
		tfNome = new JTextField();
		tfNome.setBounds(25, 50, 220, 20);
		contentPane.add(tfNome);
		tfNome.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEmail.setBounds(25, 90, 46, 14);
		contentPane.add(lblEmail);
		
		tfEmail = new JTextField();
		tfEmail.setColumns(10);
		tfEmail.setBounds(25, 115, 220, 20);
		contentPane.add(tfEmail);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSenha.setBounds(25, 157, 46, 14);
		contentPane.add(lblSenha);
		
		tfSenha = new JTextField();
		tfSenha.setColumns(10);
		tfSenha.setBounds(25, 182, 220, 20);
		contentPane.add(tfSenha);
		
		JLabel lblVoucher = new JLabel("Voucher \r\n(para professores)");
		lblVoucher.setHorizontalAlignment(SwingConstants.LEFT);
		lblVoucher.setVerticalAlignment(SwingConstants.BOTTOM);
		lblVoucher.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblVoucher.setBounds(25, 213, 220, 20);
		contentPane.add(lblVoucher);
		
		tfVoucher = new JTextField();
		tfVoucher.setColumns(10);
		tfVoucher.setBounds(25, 244, 220, 20);
		contentPane.add(tfVoucher);
		
		JButton btCadastrar = new JButton("Cadastrar");
		btCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nome = tfNome.getText();
				String email = tfEmail.getText();
				String senha = tfSenha.getText();
				String voucher =tfVoucher.getText();
				ComandosSQL.CadastrarUser(nome, email, voucher, senha);
			}
		});
		btCadastrar.setBounds(25, 290, 220, 23);
		contentPane.add(btCadastrar);
		
		JButton btVoltarLogin = new JButton("Voltar para o login");
		btVoltarLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			GerenciadorTelas.AbrirLogin();
			}
		});
		btVoltarLogin.setBounds(25, 338, 220, 23);
		contentPane.add(btVoltarLogin);

	}
}
