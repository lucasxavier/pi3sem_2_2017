package br.com.pi.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.pi.codigos.ComandosSQL;
import br.com.pi.codigos.GerenciadorTelas;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JSlider;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NovaQuests extends JFrame {

	private JPanel contentPane;
	private JTextField tfNomeQuest;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NovaQuests frame = new NovaQuests();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NovaQuests() {
		setTitle("Piolingo - Nova Quest");
		setIconImage(Toolkit.getDefaultToolkit().getImage(NovaQuests.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 352, 608);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNomeQuest = new JLabel("T\u00EDtulo da Quest");
		lblNomeQuest.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNomeQuest.setBounds(10, 11, 204, 14);
		contentPane.add(lblNomeQuest);

		tfNomeQuest = new JTextField();
		tfNomeQuest.setBounds(10, 36, 315, 20);
		contentPane.add(tfNomeQuest);
		tfNomeQuest.setColumns(10);

		JTextPane tfDescQuest = new JTextPane();
		tfDescQuest.setBounds(10, 92, 315, 135);
		contentPane.add(tfDescQuest);

		JLabel lblDescrioDaQuest = new JLabel("Descri\u00E7\u00E3o da Quest");
		lblDescrioDaQuest.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDescrioDaQuest.setBounds(10, 67, 204, 14);
		contentPane.add(lblDescrioDaQuest);

		JLabel lblMateria = new JLabel("Mat\u00E9ria");
		lblMateria.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMateria.setBounds(10, 250, 204, 14);
		contentPane.add(lblMateria);

		JComboBox cbMateria = new JComboBox();
		cbMateria.setModel(new DefaultComboBoxModel(new String[] {"POO", "Ingl\u00EAs III", "Engenharia de Software II"}));
		cbMateria.setSelectedIndex(0);
		cbMateria.setBounds(10, 268, 315, 20);
		contentPane.add(cbMateria);

		JLabel lblSemestre = new JLabel("Semestre");
		lblSemestre.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSemestre.setBounds(10, 299, 204, 14);
		contentPane.add(lblSemestre);

		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"1\u00BA", "2\u00BA", "3\u00BA", "4\u00BA", "5\u00BA", "6\u00BA"}));
		comboBox_1.setBounds(10, 317, 36, 20);
		contentPane.add(comboBox_1);

		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"2012", "2013", "2014", "2015", "2016", "2017", "2018"}));
		comboBox_2.setBounds(57, 317, 268, 20);
		contentPane.add(comboBox_2);

		JLabel lblCurso = new JLabel("Curso");
		lblCurso.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCurso.setBounds(10, 348, 204, 14);
		contentPane.add(lblCurso);

		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"Log\u00EDstica Aeroportu\u00E1ria", "Redes", "An\u00E1lise e Desenvolvimento de Sistesmas"}));
		comboBox_3.setBounds(10, 366, 315, 20);
		contentPane.add(comboBox_3);

		JComboBox cbDificuldade = new JComboBox();
		cbDificuldade.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		cbDificuldade.setSelectedIndex(0);
		cbDificuldade.setBounds(10, 415, 315, 20);
		contentPane.add(cbDificuldade);

		JLabel lblDificuldade = new JLabel("Dificuldade");
		lblDificuldade.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDificuldade.setBounds(10, 397, 204, 14);
		contentPane.add(lblDificuldade);

		JButton btnNewButton = new JButton("Criar Quest");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nomeQuest = tfNomeQuest.getText();
				String descQuest = tfDescQuest.getText();
				String dificuldade = null;
				int pontos = 0;
				int idMat = 0;

				switch (cbDificuldade.getSelectedItem().toString()) {
				case "1":
					dificuldade = "F�cil";
					pontos = 50;
					break;
				case "2":
					dificuldade = "Moderado";
					pontos = 150;
					break;
				case "3":
					dificuldade = "Dif�cil";
					pontos = 500;
					break;
				case "4":
					dificuldade = "Dark Souls";
					pontos = 1000;
					break;
				case "5":
					dificuldade = "IMPOSSIBRU";
					pontos = 5000;
					break;
				default:
					break;
				}

				String status = "Ativa";

				switch (cbMateria.getSelectedItem().toString()) {
				case "Estat�stica":
					idMat = 1; 
					break;
				case "POO":
					idMat = 2;
					break;
				case "Ingl�s III":
					idMat = 3;
					break;
				case "Engenharia de Software II":
					idMat = 4;
					break;
				default:
					break;
				}
				ComandosSQL.CriarQuest(nomeQuest, descQuest, dificuldade, pontos, status, idMat);
				GerenciadorTelas.AbrirMenu();
			}
		});
		btnNewButton.setBounds(10, 446, 315, 38);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Cancelar e voltar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GerenciadorTelas.AbrirMenu();
			}
		});
		btnNewButton_1.setBounds(10, 517, 315, 41);
		contentPane.add(btnNewButton_1);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(NovaQuests.class.getResource("/br/com/pi/imagens/oldPaper.jpg")));
		lblNewLabel.setBounds(-12, -10, 669, 609);
		contentPane.add(lblNewLabel);
	}
}
