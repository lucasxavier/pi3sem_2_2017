package br.com.pi.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.pi.codigos.ComandosSQL;
import br.com.pi.codigos.GerenciadorBD;
import br.com.pi.codigos.GerenciadorTelas;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import java.awt.Font;

public class Login extends JFrame {

	private JPanel painelBackground;
	private JTextField tfLogin;
	private JPasswordField psSenha;

	/**
	 * Create the frame.
	 */
	public Login() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Login.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setResizable(false);
		setTitle("Piolingo - Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 340, 375);
		painelBackground = new JPanel();
		painelBackground.setBackground(new Color(153, 204, 204));
		painelBackground.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(painelBackground);
		painelBackground.setLayout(null);
		
		tfLogin = new JTextField();
		tfLogin.setBounds(60, 50, 210, 20);
		painelBackground.add(tfLogin);
		tfLogin.setColumns(10);
		
		psSenha = new JPasswordField();
		psSenha.setBounds(60, 135, 210, 20);
		painelBackground.add(psSenha);
		
		JLabel lblNickname = new JLabel("Nickname");
		lblNickname.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNickname.setBounds(60, 25, 129, 14);
		painelBackground.add(lblNickname);
		
		JLabel lblSenha = new JLabel("Senha");
		lblSenha.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSenha.setBounds(60, 110, 46, 14);
		painelBackground.add(lblSenha);
		
		JButton btnLogar = new JButton("Logar");
		btnLogar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String login = tfLogin.getText();
				char[] tempSenha = psSenha.getPassword();
				String senha = new String(tempSenha);
				ComandosSQL.LogarUsuario(login, senha);				
			}
		});
		btnLogar.setBounds(120, 166, 89, 23);
		painelBackground.add(btnLogar);
		
		JButton btnCriarUsurio = new JButton("Criar Usu\u00E1rio");
		btnCriarUsurio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GerenciadorTelas.AbrirCadastro();
			}
		});
		btnCriarUsurio.setBounds(60, 260, 210, 23);
		painelBackground.add(btnCriarUsurio);
		
		JButton btnEsqueciMinhaSenha = new JButton("Esqueci minha senha");
		btnEsqueciMinhaSenha.setBounds(60, 300, 210, 23);
		painelBackground.add(btnEsqueciMinhaSenha);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Login.class.getResource("/br/com/pi/imagens/oldPaper.jpg")));
		lblNewLabel.setBounds(0, 0, 334, 471);
		painelBackground.add(lblNewLabel);
	}
}
