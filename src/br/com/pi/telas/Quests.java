package br.com.pi.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.pi.codigos.ComandosSQL;
import br.com.pi.codigos.GerenciadorTelas;

import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class Quests extends JFrame {

	private JPanel contentPane;
	public static JTable table;
	public static JButton btnCriarQuest;

	/**
	 * Create the frame.
	 */
	public Quests() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Quests.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setTitle("Piolingo - Quests");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 760, 555);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnCriarQuest =  new JButton("Criar Quest");
		btnCriarQuest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GerenciadorTelas.CadastrarQuest();
			}
		});
		btnCriarQuest.setBounds(470, 476, 244, 39);
		contentPane.add(btnCriarQuest);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 13, 734, 433);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
				new Object[][] {

				},
				new String[] {
						"T\u00EDtulo", "Descri\u00E7\u00E3o", "Dificuldade", "Status"
				}
				));
		table.getColumnModel().getColumn(0).setPreferredWidth(101);
		table.getColumnModel().getColumn(1).setPreferredWidth(188);
		table.getColumnModel().getColumn(2).setPreferredWidth(63);

		JButton btnVoltar = new JButton("Voltar ao menu");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GerenciadorTelas.AbrirMenu();
			}
		});
		btnVoltar.setBounds(10, 476, 276, 39);
		contentPane.add(btnVoltar);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(Quests.class.getResource("/br/com/pi/imagens/oldPaper.jpg")));
		lblNewLabel_1.setBounds(0, 0, 789, 580);
		contentPane.add(lblNewLabel_1);
		
		

	}

}
