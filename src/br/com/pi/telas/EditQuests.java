package br.com.pi.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JSlider;
import javax.swing.JRadioButton;
import javax.swing.JButton;

public class EditQuests extends JFrame {

	private JPanel contentPane;
	private JTextField tfNomeQuest;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditQuests frame = new EditQuests();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditQuests() {
		setTitle("Piolingo - Editar Quest");
		setIconImage(Toolkit.getDefaultToolkit().getImage(EditQuests.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 563, 478);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("T\u00EDtulo da Quest");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_1.setBounds(10, 11, 204, 14);
		contentPane.add(lblNewLabel_1);
		
		tfNomeQuest = new JTextField();
		tfNomeQuest.setBounds(10, 36, 315, 20);
		contentPane.add(tfNomeQuest);
		tfNomeQuest.setColumns(10);
		
		JTextPane tfDescQuest = new JTextPane();
		tfDescQuest.setBounds(10, 92, 315, 135);
		contentPane.add(tfDescQuest);
		
		JLabel lblDescrioDaQuest = new JLabel("Descri\u00E7\u00E3o da Quest");
		lblDescrioDaQuest.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDescrioDaQuest.setBounds(10, 67, 204, 14);
		contentPane.add(lblDescrioDaQuest);
		
		JLabel lblMatria = new JLabel("Mat\u00E9ria");
		lblMatria.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblMatria.setBounds(10, 250, 204, 14);
		contentPane.add(lblMatria);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"POO", "Ingl\u00EAs III", "Engenharia de Software II"}));
		comboBox.setSelectedIndex(0);
		comboBox.setBounds(10, 268, 315, 20);
		contentPane.add(comboBox);
		
		JLabel lblSemestre = new JLabel("Semestre");
		lblSemestre.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblSemestre.setBounds(10, 299, 204, 14);
		contentPane.add(lblSemestre);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"1\u00BA", "2\u00BA", "3\u00BA", "4\u00BA", "5\u00BA", "6\u00BA"}));
		comboBox_1.setBounds(10, 317, 36, 20);
		contentPane.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"2012", "2013", "2014", "2015", "2016", "2017", "2018"}));
		comboBox_2.setBounds(57, 317, 268, 20);
		contentPane.add(comboBox_2);
		
		JLabel lblCurso = new JLabel("Curso");
		lblCurso.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCurso.setBounds(10, 348, 204, 14);
		contentPane.add(lblCurso);
		
		JComboBox comboBox_3 = new JComboBox();
		comboBox_3.setModel(new DefaultComboBoxModel(new String[] {"Log\u00EDstica Aeroportu\u00E1ria", "Redes", "An\u00E1lise e Desenvolvimento de Sistesmas"}));
		comboBox_3.setBounds(10, 366, 315, 20);
		contentPane.add(comboBox_3);
		
		JComboBox comboBox_4 = new JComboBox();
		comboBox_4.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5"}));
		comboBox_4.setBounds(10, 415, 315, 20);
		contentPane.add(comboBox_4);
		
		JLabel lblDificuldade = new JLabel("Dificuldade");
		lblDificuldade.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDificuldade.setBounds(10, 397, 204, 14);
		contentPane.add(lblDificuldade);
		
		JButton btnNewButton = new JButton("Salvar Altera\u00E7\u00F5es");
		btnNewButton.setBounds(352, 27, 185, 70);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Cancelar e voltar");
		btnNewButton_1.setBounds(352, 385, 185, 41);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Definir Quest como Conclu\u00EDda");
		btnNewButton_2.setBounds(352, 124, 185, 70);
		contentPane.add(btnNewButton_2);
		
		JLabel lblNewLabel_2 = new JLabel("Status: em andamento");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_2.setBounds(352, 251, 185, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(EditQuests.class.getResource("/br/com/pi/imagens/oldPaper.jpg")));
		lblNewLabel.setBounds(-12, -10, 669, 609);
		contentPane.add(lblNewLabel);
	}
}
