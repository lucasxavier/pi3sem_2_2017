package br.com.pi.telas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.pi.codigos.ComandosSQL;
import br.com.pi.codigos.GerenciadorTelas;

import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class Guilda extends JFrame {

	private JPanel contentPane;
	public static JTable table;

	/**
	 * Create the frame.
	 */
	public Guilda() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Guilda.class.getResource("/br/com/pi/imagens/icons8-sword.png")));
		setTitle("Piolingo - Guilda");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 760, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 13, 469, 367);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				
			},
			new String[] {
				"Nickname", "XP"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		JLabel lblNewLabel = new JLabel("Nome da Guilda");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(500, 18, 136, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblLblnomeguilda = new JLabel("lblNomeGuilda");
		lblLblnomeguilda.setBounds(500, 43, 136, 14);
		contentPane.add(lblLblnomeguilda);
		
		JButton btnNewButton = new JButton("Atualizar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ComandosSQL.GerarRankingGuilda();
			}
		});
		btnNewButton.setBounds(500, 108, 244, 62);
		contentPane.add(btnNewButton);
		
		JButton btnRola = new JButton("Voltar ao menu");
		btnRola.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GerenciadorTelas.AbrirMenu();
			}
		});
		btnRola.setBounds(500, 339, 244, 39);
		contentPane.add(btnRola);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon(Guilda.class.getResource("/br/com/pi/imagens/oldPaper.jpg")));
		lblNewLabel_1.setBounds(-14, 0, 786, 436);
		contentPane.add(lblNewLabel_1);
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(373);
		table.getColumnModel().getColumn(1).setPreferredWidth(109);
	}
}
